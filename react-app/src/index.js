import { StrictMode } from 'react';
import './index.css';
import React from "react";
import ReactDOM from 'react-dom/client';

import { Provider } from "react-redux";
import { store } from "./reducers/store";

import App from './App';

import "./fonts/static/Montserrat-Bold.ttf";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <StrictMode>
    <Provider store={store}>
        <App />
    </Provider>
  </StrictMode>
);