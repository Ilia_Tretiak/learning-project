import Cards from "../components/Cards/Cards";

const headphones = [
    {
        id: 1,
        img: "./image/image.svg",
        title: "Apple BYZ S8521",
        price: 2927,
        rate: 4.7
    },
    {
        id: 2,
        img: "./image/image2.svg",
        title: "Apple EarPods",
        price: 2327,
        rate: 4.5
    },
    {
        id: 3,
        img: "./image/image3.svg",
        title: "Apple EarPods",
        price: 2327,
        rate: 4.5
    }
]

export default headphones;


