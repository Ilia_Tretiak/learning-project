const airHeadphones = [
    {
        id: 4,
        img: "./image/image4.svg",
        title: "Apple AirPods",
        price: 9527,
        rate: 4.7
    },
    {
        id: 5,
        img: "./image/image5.svg",
        title: "GERLAX GH-04",
        price: 6527,
        rate: 4.7
    },
    {
        id: 6,
        img: "./image/image6.svg",
        title: "BOROFONE BO4",
        price: 7527,
        rate: 4.7
    },
]

export default airHeadphones;
