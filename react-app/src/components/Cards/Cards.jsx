
import "../../style/card-image.css";
import "../../style/card.css";
import "../../style/characteristic.css";
import "../../style/my-basket.css";

import { useDispatch, useSelector } from 'react-redux';
import { setItemInCart, setCountCart } from "../../reducers/cartSlice";

function Cards({headphone}) {

    const dispatch = useDispatch();
    const items = useSelector(state => state.cart.itemsInCart);
    const isItemInCart = items.some(item => item.id === headphone.id);
  
    const addToCart = (e) => {
        e.stopPropagation();
        if ( isItemInCart ) {}
        else {dispatch(setItemInCart(headphone));
                dispatch(setCountCart())}
    }

    return (
        <>
        <div className="card__content">
            <div className="card__content_image" key={headphone.id}>
                <img className="card-image__present" src={headphone.img} alt={headphone.title}/>
            </div>
            <div className="card__group">
                <div className="card__description">
                    <span className="card-image__title">{headphone.title}</span>
                    <div className="card__rate">
                        <svg width="23.33" height="21.93" viewBox="0 0 25 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.6268 17.6614L5.41618 22.0127L7.37647 13.892L0.960754 8.462L9.38215 7.79538L12.6268 0.0867615L15.8715 7.79538L24.2941 8.462L17.8771 13.892L19.8374 22.0127L12.6268 17.6614Z" fill="#FFCE7F"/>
                        </svg>
                        <span>{headphone.rate}</span>
                    </div>
                </div>
                <div className="card__description">
                    <span className="card-image__title_price">{headphone.price.toLocaleString('ru')} ₽</span>
                    <button 
                    onClick={ addToCart }
                    className="card__button">Купить</button>
                </div>
            </div>
        </div>
        </>
    )
}

export default Cards;



