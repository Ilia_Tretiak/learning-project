import React from "react";

import "../style/whole.css";

import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import CartIndex from "./CartIndex/CartIndex";

function PageIndex() {
    return (
        <div className="whole-font">
            <main>
                <Header />
                <div className="whole-page">
                    <div>
                        <CartIndex />    
                    </div>
                </div>
            </main>
        <Footer/>
    </div>
    );
} 

export default PageIndex;