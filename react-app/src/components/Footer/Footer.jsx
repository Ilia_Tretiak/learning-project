import "../../style/footer.css"

function Footer() {
    return (
        <footer className="footer">
            <div className="footer__card">
                <div className="footer__title">QPICK
                </div>

                <div className="footer__links">
                    <div className="group11">
                        <div>Избранное</div>
                        <div>Корзина</div>
                        <div>Контакты</div>
                    </div>

                    <div className="frame12">
                    <div className="frame12__title">Условия сервиса
                    </div>
                    <div className="group28">
                        <div className="group28__ru">
                        </div>
                        {/* <div>Каз</div> */}
                        <div className="group28_selected">Рус</div>
                        <div>Eng</div>
                    </div>
                    </div>
                </div>

                <div className="footer__social">
                    <div className="vk">
                    </div>
                    <div className="telegram">
                    </div>
                    <div className="whatsapp">
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
