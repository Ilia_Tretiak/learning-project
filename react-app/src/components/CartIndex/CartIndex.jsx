import { useSelector } from "react-redux";
import CartMenu from "../CartMenu/CartMenu";
import "../../style/cart.css"

function CartIndex() {
    const items = useSelector(state => state.cart.itemsInCart)
    const totalPrice = items.reduce((acc, headphone) => acc += headphone.price, 0)
    const countItems = useSelector(state => state.cart.countItems);
    const totalPriceOfItem = totalPrice * countItems;

    return (
        <>
        <div className="characteristic-title">
            <h3 className="characteristic-title__name cart__title">Корзина </h3>
        </div>
        
        <div className="cart__group">

            <div className="cart__items">
                { items.map(headphone => (<CartMenu headphone={headphone} key={headphone.id} />))}
            </div>

            {items.length > 0 ? ( 
            <div className="cart__important">
                <div className="cart__total">
                    <div>ИТОГО</div>
                    {totalPriceOfItem >= 0 ? <div> ₽ {totalPriceOfItem.toLocaleString('ru')}</div> : ""}

                </div>
                <button className="cart__button">Перейти к оформлению</button>
            </div>)
            : <span className="cart__bottom">Корзина пуста</span>
        }
        </div>
      
        </>
        
    );
}

export default CartIndex;