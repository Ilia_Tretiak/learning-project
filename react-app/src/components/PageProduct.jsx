import React from "react";
import Products from "./Products/Products.jsx";
import Header from "./Header/Header";
import Footer from "./Footer/Footer";

import "../style/whole.css";

function PageProduct() {
    return (
    <div className="whole-font">
        <main>
            <Header />
            <div className="whole-page">
                <div>
                    <Products />   
                </div>
            </div>
        </main>
    <Footer />
</div>       
)
}


export default PageProduct;