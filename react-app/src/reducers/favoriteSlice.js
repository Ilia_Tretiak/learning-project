import { createSlice } from '@reduxjs/toolkit';

const storage = window.localStorage;

const initialState = {
  favoriteItemsCount: Number(storage.getItem('favoriteCount')) || 0,
};

export const favoriteSlice = createSlice({
  name: 'favorite',
  initialState,
  reducers: {
    ADD_TO_favorite: (state) => {
      state.favoriteItemsCount += 1;
    },
    CLEAR_favorite: (state) => {
      state.favoriteItemsCount = 0;
    },
  },
});

export const { ADD_TO_favorite, CLEAR_favorite } = favoriteSlice.actions;

export default favoriteSlice.reducer;