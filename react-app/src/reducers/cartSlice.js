import { createSlice } from '@reduxjs/toolkit';

export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    itemsInCart: [],
    countCart: 0,
    countItems: 1,
    totalPriceOfItems: 0,
  },
  reducers: {
    setItemInCart: (state, action) => {
      state.itemsInCart.push(action.payload)
    },
    deleteItemFromCart: (state, action) => {
      state.itemsInCart = state.itemsInCart.filter(headphone => headphone.id === action.payload)
    },

    setCountCart: (state) => {
      state.countCart += 1;
    },

    setCountItems: (state) => {
      state.countItems += 1;
    },

    deleteCountItems: (state) => {
      state.countItems -= 1;
    },

    setTotalPriceOfItems: (state) => {
      state.totalPriceOfItems.push(state);
    }
  },
});

export const { setItemInCart, deleteItemFromCart, setCountCart, setCountItems, deleteCountItems, setTotalPriceOfItems } = cartSlice.actions;

export default cartSlice.reducer;