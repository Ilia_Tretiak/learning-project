import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import PageProduct from "./components/PageProduct";
import PageIndex from "./components/PageIndex";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PageProduct />}/>
        <Route path="/cart" element={<PageIndex />}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
